#include <iostream>
#include <string>
#include <vector>
#include <stdexcept>

using namespace std;





int add(string numbers){

    if(numbers==""){
        return 0; //se la stringa è vuota ritorna 0;
    }
    int delPos=numbers.find("//"); //posizione del delimitatore
    string delimiter=","; //delimitatore di default
    int lpos=0; //posizione dell'ultimo numero
    if(delPos!=string::npos){
            int endPos=numbers.find("\n"); //posizione della fine delimitatore
            delimiter=numbers.substr(delPos+2,endPos-delPos-2); //delimitatore
            numbers=numbers.substr(endPos+1,numbers.length()); //stringa senza delimitatore
    }    
    
    // quantita di numeri nella stringa
    //quantity ha lo stesso valore di container.size(); è stato inserita per chiarezza
    int quantity=0; 
    vector<string> container; //vettore di stringhe che conterrà i numeri
    
    for(int i=0;i<(int)numbers.length();i++){
        if((numbers[i]==',' || numbers[i]=='\n' || numbers[i]==delimiter[0])
        && (!numbers.substr(lpos,i-lpos).empty())){
                //controlla se il carattere è un delimitatore e se il numero non è vuoto
                container.push_back(numbers.substr(lpos,i-lpos)); 
                quantity++; 
                lpos=i+1; //aggiorna la posizione dell'ultimo numero             
        }
    }
    //aggiunta dell'ultimo numero
    if(!numbers.substr(lpos,numbers.length()-lpos).empty()){
        container.push_back(numbers.substr(lpos,numbers.length()-lpos)); //aggiunge l'ultimo numero al vettore
        quantity++; 
            }
    
    //controllo che la stringa abbia solo valori numerici
    for(int i=0; i<(int)container.size();i++){
        if(container[i].find_first_not_of("-0123456789") != std::string::npos){
            return -1; //se trova un numero non valido ritorna -1
        }
    }
    //controllo che l'array abbia dimensione corretta
    
    if(quantity>0 && quantity<3){
        int sum=0;
        for(int i=0; i<(int)container.size();i++){
            int n=stoi(container[i]); //somma i numeri
            if(n<0){
                throw invalid_argument("negatives not allowed:"+ container[i]); //se trova un numero negativo lancia un'eccezione
            }else if(n>1000){
                n=0;
            }
            sum+=n;
        }
        
        return sum; //ritorna la somma
    }
    return -1; //se non riconosce un numero di numeri compreso tra 0 e 2 ritorna -1
}

int main(int argc, char ** argv){
    //Test cases
    cout<<add("")<<endl;
    cout<<add("//;\n1;2")<<endl;
    cout<<add("1,2")<<endl;
    cout<<add("1")<<endl;
    cout<<add("1,1001")<<endl;
    cout<<add("1,2,3")<<endl;
    cout<<add("1aa,2")<<endl;
    cout<<add("1,-2")<<endl;

}